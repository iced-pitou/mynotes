import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  late final TextEditingController _email;
  late final TextEditingController _login;
  late final TextEditingController _password;

  final List<Widget> pageList = [
    Column(),
    Column()
  ];
  int pageIndex = 0;

  @override
  void initState() {
    _login = TextEditingController();
    _password = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _login.dispose();
    _password.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Welcome to My Notes'),
      ),
      body: Column(
        children: [
          TextField(
            decoration: const InputDecoration(hintText: 'Login'),
            controller: _login,
          ),
          TextField(
            decoration: const InputDecoration(hintText: 'Password'),
            controller: _password,
            obscureText: true,
            enableSuggestions: false,
            autocorrect: false,
          ),
          TextButton(
            onPressed: () async {
              final login = _login.text;
              final password = _password.text;
              await _loginUser(login, password);
            },
            child: const Text('Login'),
          ),
        ],
      ),
    );
  }

  Future<int> _loginUser(String login, String password) async {
    // TODO http POST {name, password}
    return Future.delayed(const Duration(seconds: 0), () {
      _showToast(context);
      return 0;
    });
  }

  void _showToast(BuildContext context) {
    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(
      const SnackBar(
        backgroundColor: Colors.blue,
        content: Text(
          'Logged In',
          textAlign: TextAlign.center,
        ),
        duration: Duration(seconds: 2),
      ),
    );
  }
}
