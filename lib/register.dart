import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  State<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      title: const Text(
        'Validate',
        textAlign: TextAlign.left,
      ),
      children: [
        FloatingActionButton(
          onPressed: () {},
          hoverColor: Colors.lightBlue[200],
          child: const Icon(CupertinoIcons.check_mark),
        ),
      ],
    );
  }
}
